# :no_entry_sign: This repo is obsolete. :no_entry_sign: Please head to the new hosted version [here](https://github.com/ankitgaurav/Perfect-New-Tab)
# Perfect-tab
A fast extension for Google Chrome with lots of useful features with a tiny footprint.
Based on Google's Angular JS

#Screenshot
![Screenshot](https://github.com/ankitgaurav/perfect-tab/blob/gh-pages/Screenshot%202016-10-22%2005.21.36.png)

# How to use
1. Download and extract the zip of the extension from [here][download-zip].
2. Select the folder 'perfect-tab' inside the unzipped folder.
2. Go the Extensions Manager in Chrome by typing in chrome://extensions in the URL bar.
3. Drag and drop the selected folder(perfect-tab) there.

# Contributions
Comments and improvements are more than welcome!

Feel free to fork and open a pull request.
Please make your changes in a specific branch and request to pull into master!
If you can, please make sure the extensions fully works before sending the PR, as that will help speed up the process.

# License
Perfect-tab ( repository name "perfect-tab" ) is licensed under the [MIT][mit] license.

[download-zip]: https://github.com/ankitgaurav/perfect-tab/archive/master.zip
[mit]: https://github.com/ankitgaurav/perfect-tab/blob/master/LICENSE
